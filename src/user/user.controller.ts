import { UserService } from './user.service';
import {
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
} from '@nestjs/common';
import { Request } from 'express';

@Controller('/user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  getUser() {
    return this.userService.get();
  }
  @Post()
  store(@Req() req: Request) {
    return this.userService.create(req);
  }

  @Get('/:userID')
  getSingleUser(@Param() parma: { userId: number }) {
    return this.userService.getUser(parma);
  }

  @Patch('/:userId')
  updateUser(@Req() req: Request, @Param() param: { userId: number }) {
    return this.userService.update(req, param);
  }

  @Delete('/:userId')
  deleteUser(@Param() parma: { userId: number }) {
    return this.userService.delete(parma);
  }
}
