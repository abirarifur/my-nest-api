import { Injectable } from '@nestjs/common';
import { Request } from 'express';

@Injectable()
export class UserService {
  get() {
    return { name: 'I am a user', age: 25 };
  }
  getUser(param) {
    return param;
  }
  create(req: Request) {
    return req.body;
  }
  update(req: Request, parma: { userId: number }) {
    return { body: req.body, parma };
  }
  delete(parma: { userId: number }) {
    return parma;
  }
}
